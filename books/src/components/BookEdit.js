import { useState } from 'react';
import useBooksContext from '../hooks/use-books-context';

function BookEdit({ book, onSubmit }) {
    const [title, setTitle] = useState(book.title);
    const {editBookById} = useBooksContext();

    const handleChange = (event) => {
        setTitle(event.target.value);
    };

    const handleSUbmit = (event) =>{
        event.preventDefault();
        onSubmit(); //  tell bookshow when to close the form
        editBookById(book.id, title);

    };
    return (
        <form onSubmit={handleSUbmit} className="book-edit">
            <label>Title</label>
            <input className="input" value={title} onChange={handleChange}/>
            <button className="button is-primary">
                Save
            </button>
        </form>
    );
}

export default BookEdit;