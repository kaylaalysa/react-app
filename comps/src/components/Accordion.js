import {useState} from 'react';
import { GoChevronDown, GoChevronLeft } from "react-icons/go";

function Accordion({ items }) {
    //usestate(0) -> 1 collapse open
    //usestate(-1) all closed
    const [expandedIndex, setExpandedIndex] = useState(-1);


    //another variation of handleclick
    const handleClick = (nextIndex) =>{
        setExpandedIndex((currentExpandedIndex) =>{
            if(currentExpandedIndex === nextIndex){
                return -1;
            }else{
                return nextIndex;
            }
        });
        //allow user to close again
        if(expandedIndex === nextIndex){
            setExpandedIndex(-1);
        }
        else{
            setExpandedIndex(nextIndex);
        }
    };

    const renderedItems = items.map((item, index) => {
        const isExpanded = index ===expandedIndex;

        // const handleClick = () =>{
        //     setExpandedIndex(index);
        // };
        
        //displaying icon
        const icon = <span className='text-4xl'>
            {isExpanded ? <GoChevronDown/>: <GoChevronLeft/>}
        </span>

        return(
            <div key={item.id}>
                {/* <div onClick={handleClick} >{item.label}</div> */}
                <div className="flex justify-between p-3 bg-gray-50 border-b items-center cursor-pointer" onClick={() =>handleClick(index)}>
                    
                    {item.label}
                    {icon}
                </div>
                {isExpanded && <div className='border-b p-5'>{item.content}</div>}
            </div>
        );
    });
    return <div className='border-x border-t rounded'>{renderedItems}</div>;
}

export default Accordion;